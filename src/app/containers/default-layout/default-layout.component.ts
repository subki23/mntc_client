import {Component} from '@angular/core';
import { Router } from '@angular/router';
import { navItems } from '../../_nav';
// import { SocialAuthService } from 'angularx-social-login';

import { ToastrService } from 'ngx-toastr';
import { MsalService } from '@azure/msal-angular';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  public sidebarMinimized = false;
  public navItems = navItems;

  constructor(
    private router: Router,
    // private authService: SocialAuthService,
    private toastr: ToastrService,
    private authService: MsalService
  ){}

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
  
  logout = async () => {
    try {
      // await this.authService.signOut();
      // this.authService.logoutRedirect({
      //   postLogoutRedirectUri: 'http://localhost:4200'
      // }); 
      this.authService.logoutPopup({
        mainWindowRedirectUri: "/",
      });
      // this.toastr.success('Success SignOut');
      localStorage.removeItem('currentUser');
      // this.router.navigate(['login'])
    } catch (error) {
      // console.log(error)
      
    }
  }
}

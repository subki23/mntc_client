import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

// import { Login } from './model/login';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(
    private http: HttpClient,
    private toastr: ToastrService
    ) { 

  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  // public get currentUserValue(): Login {
  //   return JSON.parse(localStorage.getItem('currentUser'));
  // }

  /** POST **/
  // loginSgs(params: Login): Observable<Login> {
  //     // console.log(JSON.stringify(hero));
  //     // console.log(this.urlApi + 'getUsers')
  //       return this.http.post<any>(this.urlApi + 'apis/login', JSON.stringify(params), httpOptions)
  //       // return this.http.get<any>(this.urlApi + 'partner/companies', {})
  //           .pipe(map(user => {
  //               // login successful if there's a jwt token in the response
  //               if (user) {
  //                   const data = user;
  //                   console.log(data);
  //                   // store user details and jwt token in local storage to keep user logged in between page refreshes
  //                   localStorage.setItem('currentUser', JSON.stringify(data));
  //                   this.currentUserSubject.next(data);
  //               }

  //               return user;
  //           }));
  // }

  /** GET **/
  getAuditprojectlist(){
    return this.http.get<any>("https://audit.devcode.id:8192/audit/all", httpOptions);
  }
  getAuditprojectdetail(params:any){
    return this.http.get<any>("https://audit.devcode.id:8192/audit/"+params.auditId, httpOptions);
  }
  getAuditprojectlistparams(params:any){
    return this.http.get<any>(`https://audit.devcode.id:8192/audit/criteria?so=${params.so}&client=${params.client}&projectName=${params.projectName}&application=${params.application}&year=${params.year}&project=${params.project}&mode=${params.mode}&orderType=${params.orderType}&status=${params.status}&development=${params.development}&maintenance=${params.maintenance}&summary=${params.summary}&needToArchive=${params.needToArchive}&notes=${params.notes}&kpi=${params.kpi}`, httpOptions);
  }
  getAuditprojectlistpagingparams(params:any){
    return this.http.get<any>(`https://audit.devcode.id:8192/audit/paging?so=${params.so}&client=${params.client}&projectName=${params.projectName}&application=${params.application}&year=${params.year}&project=${params.project}&mode=${params.mode}&orderType=${params.orderType}&status=${params.status}&development=${params.development}&maintenance=${params.maintenance}&summary=${params.summary}&needToArchive=${params.needToArchive}&notes=${params.notes}&kpi=${params.kpi}&page=${params.page}&count=${params.count}&order=${params.order}&sort=${params.sort}`, httpOptions);
  }
  SetAuditprojectupload(params:any){
    return this.http.post<any>("https://audit.devcode.id:8192/audit/upload", (params), {})
            .pipe(
              tap( // Log the result or error
                data => this.toastr.success(data.meta.message),
                error => this.toastr.error(error.error.meta.message)
              )
            );
  }
  GetEstablishasanalist(){
    return this.http.get<any>("https://webhook.devcode.id/asana-webhook/projects", httpOptions)
    .pipe(
      tap( // Log the result or error
        // data => this.toastr.success(data.meta.message),
        data => "",
        error => this.toastr.error("Times out!")
        // error => console.log(error)
      )
    );
  }
  GetEstablishasanaview(projectId:any){
    return this.http.get<any>(`https://webhook.devcode.id/asana-webhook/project/${projectId}`, httpOptions)
    .pipe(
      tap( // Log the result or error
        // data => this.toastr.success(data.meta.message),
        data => "",
        error => this.toastr.error("Times out!")
        // error => console.log(error)
      )
    );
  }
  Getprojectfromasana(){
    return this.http.get<any>("https://webhook.devcode.id/asana-webhook/projectAsana", httpOptions)
    .pipe(
      tap( // Log the result or error
        // data => this.toastr.success(data.meta.message),
        data => "",
        error => this.toastr.error("Times out!")
        // error => console.log(error)
      )
    );
  }
  SetAuditprojectfromasana(params:any){
    return this.http.post<any>("https://webhook.devcode.id/asana-webhook/establishWebhook", (params), {})
            .pipe(
              tap( // Log the result or error
                data => this.toastr.success(data.meta.message),
                error => this.toastr.error(error.error.meta.message)
              )
            );
  }

}

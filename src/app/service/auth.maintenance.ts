import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
// import { setTimeout } from 'timers';
import { ApiService } from './api.service';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class AuthMaintenance implements CanActivate {
    userTimeOut:any = "";
	constructor(
        private router: Router,
        private authenticationService: ApiService,
        private toastr: ToastrService
    ) {}
    
  	canActivate(
    	next: ActivatedRouteSnapshot,
    	state: RouterStateSnapshot) {
    	const currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
        // console.log(currentUser);
        // setTimeout(() => {
        //     this.userTimeOut = currentUser;
        // }, 5000);
        // console.log(currentUser);
        if (currentUser != null) {
          const canAccess = [
            "herru@code.id",
            "adam.ichsan@code.id",
            "subki@code.id",
          ]
          if(canAccess.includes(currentUser.email)){
            return true;
          }
        }

        // not logged in so redirect to login page with the return url
        // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        this.toastr.warning("Your don't have access this page!")
        this.router.navigate(['audit-system']);
        return false;
  	}
}

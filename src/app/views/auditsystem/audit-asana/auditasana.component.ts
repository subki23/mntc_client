import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../service/api.service';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'auditasana.component.html'
})
export class AuditAsanaComponent implements OnInit {

  @ViewChild('largeModal') public largeModal: ModalDirective;

  constructor(
    private apiService: ApiService,
  ) { 
  }
  
  canActive:boolean = false
  Object = Object;
  loadingIcons:any = {}
  Listdata:any = {}
  selectedWorkspace:string
  dataview:any = {}

  ceklogin() {
    const currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    var canAccess = [
      "herru@code.id",
      "adam.ichsan@code.id",
      "subki@code.id",
    ]
    if(canAccess.includes(currentUser.account.username)){
      this.canActive = true
      this.GetEstablishasanalist()
    }else{
      this.canActive = false
    }
  }

  ngOnInit() {
    this.loadingIcons = {
      list: false,
    }
    this.ceklogin()
  }

  GetEstablishasanalist():void {
    try {
      this.Listdata = {}
      this.loadingIcons.list = true
      this.apiService.GetEstablishasanalist()
        .subscribe((response:any)=>{
          if (response.meta.isSuccess) {
            let Listdata = response.data;
            Listdata.forEach((item:any) => {
              if (!this.Listdata[item.workspaceName]) {
                this.Listdata[item.workspaceName] = []
              }
              this.Listdata[item.workspaceName].push(item)
            });
            console.log(this.Listdata)
          }else{
            console.log(response)
          }
          this.loadingIcons.list = false
        })
    } catch (error) {
      console.log(error)
    }
  }

  selectDataview(data:any):void {
    this.apiService.GetEstablishasanaview(data.projectId)
        .subscribe((response:any)=>{
          if (response.meta.isSuccess) {
            this.dataview = response.data
            this.dataview['dataMessage'] = [
              {
                "username": "rodentina",
                "message": "comment 1",
                "dateCreated":"2022-02-23 10:10:11"
              },
              {
                "username": "rodentina",
                "message": "per tanggal 4 Feb,  dari saya dan mas Jeffry. saat ini: menunggu API history langganan dari pihak melon menunggu API payment pulsa non telkomsel dari pihak melon memperbaiki ui versi mobile untuk ui versi web sudah sesuai. terakhir kita nyoba testing dengan akun indihome Mba Lida sampai selesai nyoba berlangganan sudah bisa.",
                "dateCreated":"2022-03-02 10:10:11"
              },
              {
                "username": "rodentina",
                "message": "comment 3",
                "dateCreated":"2022-03-03 10:10:11"
              },
            ]
          }else{
            console.log(response)
          }
        })
  }

}

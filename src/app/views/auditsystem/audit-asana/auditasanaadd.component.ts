import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../service/api.service';

@Component({
  templateUrl: 'auditasanaadd.component.html'
})
export class AuditAsanaAddComponent {

  constructor(
    private apiService: ApiService,
    private toastr: ToastrService
  ) { 
  }

  loadingIcons:any = {}
  source:any = "";
  selectall:any = false;
  Listfromasana:any = [];
  Listselected:any = [];

  ngOnInit() {
    this.loadingIcons = {
      listasana: false,
      submitasana: false
    }
  }

  Selectsource(source:any):void {
    if (source === 'byasana') {
      this.Getprojectfromasana()
    }else if (source === 'byform') {
      
    }
  }

  Getprojectfromasana():void {
    try {
      this.loadingIcons.listasana = true
      this.apiService.Getprojectfromasana()
        .subscribe((response:any)=>{
          if (response.meta.isSuccess) {
            this.Listfromasana = response.data
          }else{
            console.log(response)
          }
          this.loadingIcons.listasana = false
        })
    } catch (error) {
      console.log(error)
    }
    // this.Listfromasana = [
    //         {
    //             "projectId": "1201857498437145",
    //             "projectName": "C22003 - Test App1 - CR 01",
    //             "workspaceId": "1201857412858035",
    //             "workspaceName": "MELON INDONESIA",
    //             "projectIdClockify": null,
    //             "createdBy": null,
    //             "createdDate": null,
    //             "updatedBy": null,
    //             "updatedDate": null,
    //             "asanaWebhookEstablish": null
    //         },
    //         {
    //             "projectId": "1201858550953225",
    //             "projectName": "C22006 - Test App1 - CR 01",
    //             "workspaceId": "1201857498437154",
    //             "workspaceName": "CODE DEVELOPMENT",
    //             "projectIdClockify": null,
    //             "createdBy": null,
    //             "createdDate": null,
    //             "updatedBy": null,
    //             "updatedDate": null,
    //             "asanaWebhookEstablish": null
    //         },
    //         {
    //             "projectId": "1201969586817996",
    //             "projectName": "test",
    //             "workspaceId": "1201857498437154",
    //             "workspaceName": "CODE DEVELOPMENT",
    //             "projectIdClockify": null,
    //             "createdBy": null,
    //             "createdDate": null,
    //             "updatedBy": null,
    //             "updatedDate": null,
    //             "asanaWebhookEstablish": null
    //         }
    //     ]
    this.Listfromasana.forEach((item:any) => {
      item['selected'] = false
    });
  }

  Selectallasana(valueselect:any):void {
    this.Listfromasana.forEach((item:any) => {
      item['selected'] = valueselect
    });
  }

  Addbyasana():void {
    this.Listselected = []
    this.Listfromasana.forEach((item:any)=>{
      if (item.selected === true) {
        this.Listselected.push(item.projectId)
      }
    })
    // console.log(this.Listselected)
    try {
      this.loadingIcons.submitasana = true
      this.apiService.SetAuditprojectfromasana(this.Listselected)
        .subscribe((response:any)=>{
          if (response.meta.isSuccess) {
            this.toastr.success(response.data)
          }else{
            console.log(response)
          }
          this.loadingIcons.submitasana = false
        })
    } catch (error) {
      console.log(error)
    }
    // var response = {
    //     "meta": {
    //         "isSuccess": true,
    //         "status": "OK",
    //         "message": "OK",
    //         "timestamp": "30-03-2022 10:34:18"
    //     },
    //     "data": "Projects Webhook Successfully Establish"
    // }
    // if (response.meta.isSuccess) {
    //   this.toastr.success(response.data)
    // }else{
    //   console.log(response)
    //   // this.toastr.error(error.error.meta.message)
    // }
    this.source = ""
    
  }

}

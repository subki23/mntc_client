import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../service/api.service';
import { MsalBroadcastService, MsalService } from '@azure/msal-angular';
import { EventMessage, EventType, InteractionStatus } from '@azure/msal-browser';
import { filter } from 'rxjs/operators';

@Component({
  templateUrl: 'auditproject.component.html'
})
export class AuditProjectComponent implements OnInit {

  constructor(
    private apiService: ApiService,
    private authService: MsalService, 
    private msalBroadcastService: MsalBroadcastService
  ) { 
  }

  Data_audit_project:any = []
  Data_audit_paging:any = {}
  Criteria:any = {}
  Vartimeout:any
  canActive:boolean = false
  showAll:boolean = false
  hideLoadmore:boolean = false
  isCollapsed:boolean = true

  ceklogin() {
    const currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    var canAccess = [
      "herru@code.id",
      "adam.ichsan@code.id",
      "subki@code.id",
    ]
    if(canAccess.includes(currentUser.account.username)){
      this.canActive = true
    }else{
      this.canActive = false
    }
  }
  

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  ngOnInit() {
    this.msalBroadcastService.msalSubject$
      .pipe(
        filter((msg: EventMessage) => msg.eventType === EventType.LOGIN_SUCCESS),
      )
      .subscribe((result: EventMessage) => {
        console.log(result);
      });
    this.ceklogin()
    // this.Criteria = {
    //   so:"",
    //   client:"",
    //   projectName:"",
    //   application:"",
    //   year:"",
    //   project:"",
    //   mode:"",
    //   orderType:"",
    //   status:"",
    //   development:"",
    //   maintenance:"",
    //   summary:"",
    //   needToArchive:"",
    //   notes:"",
    //   kpi:"",
    //   page:0,
    //   count:15,
    //   order:"ASC",
    //   sort:"auditId",
    // }
    // if (this.showAll === false) {
    //   this.Getauditbypaging()
    // }else{
    //   this.Getauditall()
    // }
    this.Clearcriteria()
  }

  Getauditall = () => {
    try {
      this.apiService.getAuditprojectlist()
        .subscribe((response: any) => {
          var data = response.data
          data.map((item:any)=>{
            item['show_detail'] = false
            item['isCollapsed'] = true
            this.Data_audit_project.push(item)
          })
        })
    } catch (error) {
      console.log(error)
    }
  }

  Getbycriteria = () => {
    clearTimeout(this.Vartimeout)
    this.Vartimeout = setTimeout(() => {
      this.Data_audit_project = []
      this.Criteria['page'] = 0
      if (this.showAll === false) {
        this.Getauditbypaging()
      }else{
        this.Getauditbycriteria()
      }
    }, 1000);
  }

  Getauditbycriteria = () => {
    try {
      var params = {
        so:this.Criteria.so,
        client:this.Criteria.client,
        projectName:this.Criteria.projectName,
        application:this.Criteria.application,
        year:this.Criteria.year,
        project:this.Criteria.project,
        mode:this.Criteria.mode,
        orderType:this.Criteria.orderType,
        status:this.Criteria.status,
        development:this.Criteria.development,
        maintenance:this.Criteria.maintenance,
        summary:this.Criteria.summary,
        needToArchive:this.Criteria.needToArchive,
        notes:this.Criteria.notes,
        kpi:this.Criteria.kpi
      }
      this.apiService.getAuditprojectlistparams(params)
        .subscribe((response: any) => {
          var data = response.data
          data.map((item:any)=>{
            item['show_detail'] = false
            item['isCollapsed'] = true
            this.Data_audit_project.push(item)
          })
        })
    } catch (error) {
      console.log(error)
    }
  }

  Getauditbypaging = () => {
    try {
      var params = {
        so:this.Criteria.so,
        client:this.Criteria.client,
        projectName:this.Criteria.projectName,
        application:this.Criteria.application,
        year:this.Criteria.year,
        project:this.Criteria.project,
        mode:this.Criteria.mode,
        orderType:this.Criteria.orderType,
        status:this.Criteria.status,
        development:this.Criteria.development,
        maintenance:this.Criteria.maintenance,
        summary:this.Criteria.summary,
        needToArchive:this.Criteria.needToArchive,
        notes:this.Criteria.notes,
        kpi:this.Criteria.kpi,
        page:this.Criteria.page,
        count:this.Criteria.count,
        order:this.Criteria.order,
        sort:this.Criteria.sort,
      }
      this.apiService.getAuditprojectlistpagingparams(params)
        .subscribe((response: any) => {
          var data = response.data.content
          data.map((item:any)=>{
            item['show_detail'] = false
            item['isCollapsed'] = true
            this.Data_audit_project.push(item)
          })
          this.Data_audit_paging['totalPages'] = response.data.totalPages
          if ( this.Criteria['page'] >= response.data.totalPages-1) {
            this.showAll = !this.showAll
          }
        })
    } catch (error) {
      console.log(error)
    }
  }

  Clearcriteria = () => {
    this.Criteria = {
      so:"",
      client:"",
      projectName:"",
      application:"",
      year:"",
      project:"",
      mode:"",
      orderType:"",
      status:"",
      development:"",
      maintenance:"",
      summary:"",
      needToArchive:"",
      notes:"",
      kpi:"",
      page:0,
      count:15,
      order:"ASC",
      sort:"auditId",
    }
    this.showAll = false
    this.Getbycriteria()
  }

  Loadmore = () => {
    this.Criteria['page']++
    this.Getauditbypaging()
  }

  Showalldata = () => {
    this.showAll = !this.showAll
    this.Getbycriteria() 
    // if (this.showAll) {
    //   this.Getbycriteria()      
    // }else{
    //   // this.Clearcriteria()
    // }
  }

  Showdetail = (item:any) => {
    if (item.show_detail === false) {
      this.getDetail(item)
    }else{
      item.show_detail = !item.show_detail
    }
  }
  
  getDetail = (item:any) => {
    try {
      var params = {
        "auditId": item.auditId
      }
      this.apiService.getAuditprojectdetail(params)
        .subscribe((response: any) => {
          item["detail"] = response.data
          item.show_detail = !item.show_detail
        })
    } catch (error) {
      console.log(error)
    }
  }

}

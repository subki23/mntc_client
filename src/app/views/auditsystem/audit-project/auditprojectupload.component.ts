import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../service/api.service';

@Component({
  templateUrl: 'auditprojectupload.component.html'
})
export class AuditProjectUploadComponent {

  constructor(
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) { 
  }

  uploadForm: FormGroup; 
  loadingIcons:boolean = false

  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      fileaudit: ['']
    });
  }

  onFileSelect(event:any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('fileaudit').setValue(file);
    }
  }
  
  Uploadaudit = () => {
    this.loadingIcons = true
    try {
      if (!this.uploadForm.get('fileaudit').value) {
        this.toastr.error("File cannot be blank!")
        this.loadingIcons = false
        return
      }
      const formData = new FormData();
      formData.append('file', this.uploadForm.get('fileaudit').value);
      this.apiService.SetAuditprojectupload(formData)
        .subscribe((response:any)=>{
          this.loadingIcons = false
        })
    } catch (error) {
      console.log(error)
      this.loadingIcons = true
    }
  }

}

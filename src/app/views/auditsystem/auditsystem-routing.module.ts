import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthMaintenance } from '../../service/auth.maintenance';

import { AuditProjectComponent } from './audit-project/auditproject.component';
import { AuditProjectUploadComponent } from './audit-project/auditprojectupload.component';

import { AuditAsanaComponent } from './audit-asana/auditasana.component';
import { AuditAsanaAddComponent } from './audit-asana/auditasanaadd.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Audit System'
    },
    children: [
      {
        path: '',
        redirectTo: 'audit-asana'
      },
      {
        path: 'audit-project',
        component: AuditProjectComponent,
        data: {
          title: 'Audit Project'
        }
      },
      {
        path: 'audit-project-upload',
        component: AuditProjectUploadComponent,
        canActivate: [AuthMaintenance],
        data: {
          title: 'Audit Project Upload'
        }
      },
      {
        path: 'audit-asana',
        component: AuditAsanaComponent,
        data: {
          title: 'Audit Asana'
        }
      },
      {
        path: 'audit-asana-add',
        component: AuditAsanaAddComponent,
        data: {
          title: 'Audit Asana Add'
        }
      },
    ]
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditsytemRoutingModule {}

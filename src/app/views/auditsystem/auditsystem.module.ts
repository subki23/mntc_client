import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AuditProjectComponent } from './audit-project/auditproject.component';
import { AuditProjectUploadComponent } from './audit-project/auditprojectupload.component';
import { AuditAsanaComponent } from './audit-asana/auditasana.component';
import { AuditAsanaAddComponent } from './audit-asana/auditasanaadd.component';

// Components Routing
import { AuditsytemRoutingModule } from './auditsystem-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AuditsytemRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CollapseModule.forRoot(),
    ModalModule.forRoot()
  ],
  declarations: [ 
    AuditProjectComponent,
    AuditProjectUploadComponent,
    AuditAsanaComponent,
    AuditAsanaAddComponent
  ]
})
export class AuditSystemModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MsalService } from '@azure/msal-angular';
// import { GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  isIframe = false;
  loginDisplay = false;
  
  constructor(
    private router: Router,
    // private authService: SocialAuthService,
    private toastr: ToastrService,
    private authService: MsalService
  ){}

  auth:any = {
    email: '',
    password: ''
  }

  ngOnInit():void {
    this.isIframe = window !== window.parent && !window.opener;
    this.ceklogin()
  }

  signInGoogleHandler(): void {
    try {
      // this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((data)=>{
      //   var email = data.email
      //   if ( email.includes("@code.id") ) {
      //     this.toastr.success(`Wellcome ${data.name}`);
      //     localStorage.setItem("currentUser", JSON.stringify(data));
      //   }else{
      //     this.toastr.warning('Please use account Code.id');
      //     this.authService.signOut();
      //   }
      //   this.ceklogin()
      // })
    } catch (e) {
      console.log(e);
    }
  }

  ceklogin() {
    const currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser != null) {
      this.router.navigate(['']);
    }else{
      this.router.navigate(['login'])
    }
  }

  loginManual() {
    var email = this.auth.email
    if ( email.includes("@code.id") ) {
      if (this.auth.password != "code1d") {
      this.toastr.warning('Password Not Matching!!');
      // this.authService.signOut();
      }else{
        this.toastr.success(`Wellcome ${this.auth.email}`);
        localStorage.setItem("currentUser", JSON.stringify(this.auth));
      }
    }else{
      this.toastr.warning('Please use account Code.id');
      // this.authService.signOut();
    }
    this.ceklogin()
  }

  login() {
    this.authService.loginPopup()
      .subscribe({
        next: (result) => {
          console.log(result.account.username);
          // this.setLoginDisplay();
          var email = result.account.username
          var name = result.account.name
          var data = result;
          if ( email.includes("@code.id") ) {
            this.toastr.success(`Wellcome ${name}`);
            localStorage.setItem("currentUser", JSON.stringify(data));
          }else{
            this.toastr.warning('Please use account Code.id');
            // this.authService.signOut();
          }
          this.ceklogin()
        },
        error: (error) => console.log(error)
      });
  }

  setLoginDisplay() {
    this.loginDisplay = this.authService.instance.getAllAccounts().length > 0;
  }

}
